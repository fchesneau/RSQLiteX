# RSQLiteX
An extension of RSQLite that adds a host of new features

RSQLite builds on RSQLite and adds several convenience features.
A new class "RSQLiteX" (inheriting from RSQLite connection class) contains new slots:
- tables
  - name
  - number of rows
  - number of columns (?)
- fields
- ...

New functions:
...

New methods:
...
